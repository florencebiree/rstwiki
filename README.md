# RstWiki - a simple site builder

What's that?
============

Less than a usual wiki… This django application provide a simple way to build
a static website, where pages are stored as Rst or Html files in your server.

Dependencies
============

 - Python 3
 - Django 2.2
 - python3-docutils

How to use it
=============

Configure your settings.py using the provided model.

Then, in your pages_dir, you can put .rst or .html files like:

    index.rst   is the home page
    doc.html    is the /doc page
    doc/1.rst   is the /doc/1 page

There is also some special pages, included on all others:

    navbar.{html,rst}   is the navigation bar
    footer.{html,rst}   is the footer
    head.html           is some meta included in the <head> (your css, etc)

Those files can be at any level, and apply for this level.
