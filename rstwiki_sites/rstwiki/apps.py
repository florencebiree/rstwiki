from django.apps import AppConfig


class RstwikiConfig(AppConfig):
    name = 'rstwiki'
