# -*- coding: utf-8 -*-
###############################################################################
#       rstwiki/models.py
#       
#       Copyright © 2010-2020, Florence Birée <florence@biree.name>
#       
#       This file is a part of rstwiki
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public
#       License along with this program.  If not, see
#       <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Models for rstwiki"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "AGPLv3+"
__copyright__ = "Copyright © 2010-2020, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from os import path
from docutils import core
from datetime import datetime
from html.parser import HTMLParser

#from django.conf import settings

class RstPage:
    """A rstwiki page"""
    
    def __init__(self, site, pagepath):
        """Initialize a new rstwiki page"""
        self.pagepath = pagepath
        self._html_body = None
        self._title = None
        self.html = False
        self.site = site
    
    def normalized_path(self, source=False, add_ext_if_needed=True):
        """Return the normalized path of the page"""
        pagepath = self.pagepath
        # fix trailing slashs
        if source and pagepath.endswith('/'):
            pagepath = pagepath[:-1]
        elif not (source or pagepath == '' or pagepath.endswith('/')):
            pagepath += '/'
        # index: normalize    
        if source and pagepath == '':
            pagepath = 'index'
        elif not source and pagepath == 'index/':
            pagepath = ''
        # source : ends with .rst
        if source and add_ext_if_needed:
            pagepath += '.rst'
        return pagepath
    
    def has_normalized_path(self, source=False):
        """Return if the pagepath is normalized"""
        return (self.pagepath == self.normalized_path(source, False))
    
    def source_path(self):
        """Return the source path without the extension"""
        return self.normalized_path(source=True, add_ext_if_needed=False)
    
    def real_path(self):
        """Return the path of the page on the filesystem (or None if invalid)"""
        real_pagepath = path.abspath(path.join(
            self.site['pages_dir'],
            self.normalized_path(source=True)
        ))
        if not real_pagepath.startswith(path.abspath(self.site['pages_dir'])):
            return None
        return real_pagepath
    
    def exists(self):
        """Return if the page exists on the filesystem"""
        return path.isfile(self.real_path())
    
    def _get_content(self):
        """Return the reST content of the file"""
        content = ""
        try:
            with open(self.real_path(), 'r') as rstfile:
                content = rstfile.read()
        except IOError:
            return ""
        else:
            return content
    content = property(fget=_get_content) #, fset=_set_content)
    
    def _render_html(self):
        """Do the page rendering in html"""
        parts = core.publish_parts(
            source=self.content,
            writer_name='html')
        #self._html_body = parts['body_pre_docinfo'] + parts['fragment']
        #self._html_body = parts['fragment']
        self._title = parts['title']
        self._html_body = parts['html_body']
    
    def html_content(self):
        """Return the html content of the page"""
        if self._html_body is None:
            self._render_html()
        return self._html_body
    
    def title(self):
        """Return the title of the page"""
        if self._title is None:
            self._render_html()
        return self._title
    
    def mtime(self):
        """Return the last modification time"""
        if self.exists():
            return datetime.fromtimestamp(path.getmtime(self.real_path()))
        else:
            return None

class HtmlPage:
    """A rstwiki html page"""
    
    def __init__(self, site, pagepath):
        """Initialize a new rstwiki page"""
        self.pagepath = pagepath
        self._html_body = None
        self._title = None
        self.html = True
        self.site = site
    
    def normalized_path(self, source=False, add_ext_if_needed=True):
        """Return the normalized path of the page"""
        pagepath = self.pagepath
        # fix trailing slashs
        if source and pagepath.endswith('/'):
            pagepath = pagepath[:-1]
        elif not (source or pagepath == '' or pagepath.endswith('/')):
            pagepath += '/'
        # index: normalize    
        if source and pagepath == '':
            pagepath = 'index'
        elif not source and pagepath == 'index/':
            pagepath = ''
        # source : ends with .html
        if source and add_ext_if_needed:
            pagepath += '.html'
        return pagepath
    
    def has_normalized_path(self, source=False):
        """Return if the pagepath is normalized"""
        return (self.pagepath == self.normalized_path(source, False))
    
    def source_path(self):
        """Return the source path without the extension"""
        return self.normalized_path(source=True, add_ext_if_needed=False)
    
    def real_path(self):
        """Return the path of the page on the filesystem (or None if invalid)"""
        real_pagepath = path.abspath(path.join(
            self.site['pages_dir'], 
            self.normalized_path(source=True)
        ))
        if not real_pagepath.startswith(path.abspath(self.site['pages_dir'])):
            return None
        return real_pagepath
    
    def exists(self):
        """Return if the page exists on the filesystem"""
        return path.isfile(self.real_path())
    
    def _get_content(self):
        """Return the html content of the file"""
        content = ""
        try:
            with open(self.real_path(), 'r') as rstfile:
                content = rstfile.read()
        except IOError:
            return ""
        else:
            return content
    content = property(fget=_get_content) #, fset=_set_content)
        
    def html_content(self):
        """Return the html content of the page"""
        if self._html_body is None:
            self._html_body = self.content
        return self._html_body
    
    def title(self):
        """Return the title of the page"""
        if self._title is None:
            parser = H1Extractor()
            parser.feed(self.html_content())
            self._title = parser.title
        return self._title
    
    def mtime(self):
        """Return the last modification time"""
        if self.exists():
            return datetime.fromtimestamp(path.getmtime(self.real_path()))
        else:
            return None

def get_page(site, pagepath, searchpath=None):
    """Try to get a page, either RstPage or HtmlPage
    
    If searchpath is not None, search the page `pagepath` in
    searchpath, thin in each parent directories
    """
    if searchpath is not None and searchpath != '':
        comp = searchpath.split('/')
        while comp:
            pagep = '/'.join(comp + [pagepath])
            page = RstPage(site, pagep)
            if page.exists():
                return page
            page = HtmlPage(site, pagep)
            if page.exists():
                return page
            comp = comp[:-1]
    
    page = RstPage(site, pagepath)
    if page.exists():
        return page
    else:
        return HtmlPage(site, pagepath)

class H1Extractor(HTMLParser):
    """HTML Parser to extract the title of an HTML page"""
    
    def __init__(self):
        """Initialize a new parser"""
        HTMLParser.__init__(self)
        self._h1found = False
        self._inh1 = False
        self.title = ''
    
    def handle_starttag(self, tag, attrs):
        if tag == 'h1' and not self._h1found:
            self._h1found = True
            self._inh1 = True
    
    def handle_endtag(self, tag):
        if self._inh1 and tag == 'h1':
            self._inh1 = False
    
    def handle_data(self, data):
        if self._inh1:
            self.title += data
