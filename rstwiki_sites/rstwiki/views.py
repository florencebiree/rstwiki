# -*- coding: utf-8 -*-
###############################################################################
#       rstwiki/views.py
#       
#       Copyright © 2010-2020, Florence Birée <florence@biree.name>
#       
#       This file is a part of rstwiki
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public
#       License along with this program.  If not, see
#       <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Views for rstwiki"""

__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "AGPLv3+"
__copyright__ = "Copyright © 2010-2020, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from os import path
from django.shortcuts import render
from django.urls import reverse

from django.http import Http404, HttpResponse
from django.http import HttpResponseRedirect, HttpResponsePermanentRedirect
from django.conf import settings

from rstwiki.models import RstPage, HtmlPage, get_page

def _site(request):
    """Return the current site, or a fake site if Site is not used"""
    for site in settings.SITES:
        if site['domain'] == request.get_host():
            return site
    raise Http404

def render_page(request, pagepath):
    """Build the html view of the page pagepath"""
    site = _site(request)
    page = get_page(site, pagepath)
    # redirect to normalize the path
    if not page.has_normalized_path():
        return HttpResponsePermanentRedirect(reverse(
            render_page, args=[page.normalized_path()]
        ))
        #return HttpResponse(page.normalized_path())
    # Get objects
    if page.real_path == None: # invalid path
        raise Http404
    head = get_page(site, 'head/', pagepath)
    navbar = get_page(site, 'navbar/', pagepath)
    footer = get_page(site, 'footer/', pagepath)
    # Fill the template
    context = {
        'site': site,
        'page': page,
    }
    if head.exists():
        context['head'] = head
    if navbar.exists():
        context['navbar'] = navbar
    if footer.exists():
        context['footer'] = footer
    
    if page.exists():
        return render(request, 'rst_page.html', context)
    else:
        raise Http404

def page_not_found(request, exception=None):
    """Return a page not found page"""
    site = _site(request)
    # Get objects
    head = get_page(site, 'head/')                                          
    navbar = get_page(site, 'navbar/')                                           
    footer = get_page(site, 'footer/')                                           
    # Fill the template                                                          
    context = {'site': site}                                                                            
    if head.exists():                                                         
        context['head'] = head                                             
    if navbar.exists():                                                          
        context['navbar'] = navbar                                               
    if footer.exists():                                                          
        context['footer'] = footer
    return render(request, 'rst_notexist.html', context)
