��          \      �       �      �   �  �      j     w  
   �  3   �     �  A  �        �  <     �     �     �  4        :                                       
	This page does not exist.
 
This site swing thanks to <a href="http://www.djangoproject.com/"
hreflang="en" title="The Django framework">Django</a>! It run an unmodified
version of <a href="https://framagit.org/flobiree/rstwiki">rstwiki
</a>, which is released under the terms of the <a
href="http://www.gnu.org/licenses/agpl.html"><acronym title="GNU's Not Unix">
GNU</acronym> Affero General Public License</a>.
 Back to home Last change on Page title There is nothing here for the moment. See you soon. This page does not exist Project-Id-Version: 0.3
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Florence Birée <florence@biree.name>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 
	Cette page n'existe pas.
 
Ce site swingue grâce à <a href="http://www.djangoproject.com/"
hreflang="en" title="The Django framework">Django</a> ! Il utilise une version non-modifiée de <a href="https://framagit.org/flobiree/rstwiki">rstwiki
</a>, qui est publié sous les termes de la <a
href="http://www.gnu.org/licenses/agpl.html">Licence Publique Générale <acronym title="GNU's Not Unix">GNU</acronym> Affero</a>.
 Retour à l'accueil Dernier changement le Titre de la page Il n'y a rien ici pour le moment, revenez bientôt ! Cette page n'existe pas 