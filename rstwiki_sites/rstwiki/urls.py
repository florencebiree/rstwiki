# -*- coding: utf-8 -*-
###############################################################################
#       rstwiki/urls.py
#       
#       Copyright © 2010-2020, Florence Birée <florence@biree.name>
#       
#       This file is a part of rstwiki
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU Affero General Public License as
#       published by the Free Software Foundation, either version 3 of the
#       License, or (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU Affero General Public
#       License along with this program.  If not, see
#       <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""rstwiki_sites URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
__author__ = "Florence Birée"
__version__ = "0.3"
__license__ = "AGPLv3+"
__copyright__ = "Copyright © 2010-2020, Florence Birée"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

from django.contrib import admin
from django.urls import include, re_path, path
from rstwiki import views

urlpatterns = [
    #path('404/', views.page_not_found), #Test only
    re_path('^(?P<pagepath>.*)$', views.render_page, name='render'),
]
